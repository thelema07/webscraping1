import * as express from "express";
import { getJobsFromCraiglist } from "./puppeteer/lesson2";
import { scrapeCraigsList } from "./puppeteer/lesson3";
import { mongoDbConnection } from "./config/db-connection";

mongoDbConnection();
const app = express();

app.get("/", (req, res) => {
  res.send("Hello World");
});

// getJobsFromCraiglist();
scrapeCraigsList();

const PORT = process.env.PORT || 7000;

app.listen(PORT, () => {
  console.log(`Server is running in http://localhost:${PORT}`);
});
