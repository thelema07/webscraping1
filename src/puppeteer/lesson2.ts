import * as puppeteer from "puppeteer";
import * as cheerio from "cheerio";
import Listing, { IListings } from "../model/Listings";

export const mainPuppeteer = async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto("https://www.google.com");
};

export const mainCraiglist = async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(
    "https://sfbay.craigslist.org/d/software-qa-dba-etc/search/sof"
  );
  const html = await page.content();
  const $ = cheerio.load(html);
  // $(".result-title").each((index, element) => console.log($(element).text()));
  // $(".result-title").each((index, element) =>
  //   console.log($(element).attr("href"))
  // );
  const results = $(".result-info")
    .map((index, element) => {
      const titleElement = $(element).find(".result-title");
      const timeElement = $(element).find(".result-date");
      const hoodElement = $(element).find(".result-hood");
      const title = $(titleElement).text();
      const url = $(titleElement).attr("href");
      const datePosted = new Date($(timeElement).attr("datetime"));
      const hood = $(hoodElement)
        .text()
        .trim()
        .replace("(", "")
        .replace(")", "");
      return { title, url, datePosted, hood };
    })
    .get();

  console.log(results);
};

export const scrapeListings = async page => {
  await page.goto(
    "https://sfbay.craigslist.org/d/software-qa-dba-etc/search/sof"
  );
  const html = await page.content();
  const $ = cheerio.load(html);

  const listings = $(".result-info")
    .map((index, element) => {
      const titleElement = $(element).find(".result-title");
      const timeElement = $(element).find(".result-date");
      const hoodElement = $(element).find(".result-hood");
      const title = $(titleElement).text();
      const url = $(titleElement).attr("href");
      const datePosted = new Date($(timeElement).attr("datetime"));
      const hood = $(hoodElement)
        .text()
        .trim()
        .replace("(", "")
        .replace(")", "");
      return { title, url, datePosted, hood };
    })
    .get();

  return listings;
};

export const scrapeJobDescriptions = async (listings, page) => {
  for (let i = 0; i < listings.length; i++) {
    await page.goto(listings[i].url);
    const html = await page.content();
    const $ = cheerio.load(html);
    const jobDescription = $("#postingbody")
      .text()
      .trim();
    const compensation = $("p.attrgroup > span:nth-child(1) > b")
      .text()
      .trim();
    listings[i].jobDescription = jobDescription;
    listings[i].compensation = compensation;
    console.log(listings[i].compensation);
    const listingModel = new Listing(listings[i]);
    await listingModel.save();
    await sleep(1000);
  }
};

async function sleep(miliseconds) {
  return new Promise(resolve => setTimeout(resolve, miliseconds));
}

export const getJobsFromCraiglist = async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  const listings = await scrapeListings(page);
  const listingsWithJobDescription = await scrapeJobDescriptions(
    listings,
    page
  );

  console.log(listings);
};
