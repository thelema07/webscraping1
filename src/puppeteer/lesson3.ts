import * as request from "request-promise";
import * as cheerio from "cheerio";

const url = "https://www.lanacion.com.ar";

export const scrapeCraigsList = async () => {
  try {
    const htmlResult = await request.get(url);
    console.log(htmlResult);
  } catch (error) {
    console.log(error);
  }
};
