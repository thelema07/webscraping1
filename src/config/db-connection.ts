import * as mongoose from "mongoose";

export const mongoDbConnection = async () => {
  await mongoose.connect(
    "mongodb+srv://dannyHotCosaRica:dannyHotCosaRica@cluster0-0j7sd.mongodb.net/test?retryWrites=true&w=majority",
    { useNewUrlParser: true, useUnifiedTopology: true }
  );
  console.log("Connection to Mongo successful");
};
