import * as request from "request-promise";
import * as fs from "fs";
import * as cheerio from "cheerio";

const getData = async (url: string) => {
  const html = await request.get(url);
  fs.writeFileSync("./test.html", html);
  return cheerio.load(html);
};

export const main1 = async () => {
  const $ = await getData("https://reactnativetutorial.net/css-selectors/");
  const theText = $("h1").text();
  console.log(theText);
};

export const main2 = async () => {
  const $ = await getData(
    "https://reactnativetutorial.net/css-selectors/lesson2.html"
  );
  $("h2").each((index, element) => {
    console.log($(element).text());
  });
};

export const main3 = async () => {
  const $ = await getData(
    "https://reactnativetutorial.net/css-selectors/lesson5.html"
  );
  $(".red").each((index, element) => {
    console.log($(element).text());
  });
};
