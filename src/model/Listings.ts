import * as mongoose from "mongoose";

export interface IListings extends mongoose.Document {
  title: string;
  datePosted: Date;
  neighborhood: string;
  url: string;
  jobDescription: string;
  compensation: string;
}

const ListingsSchema: mongoose.Schema = new mongoose.Schema({
  title: { type: String },
  datePosted: { type: Date },
  neighborhood: { type: String },
  url: { type: String },
  jobDescription: { type: String },
  compensation: { type: String }
});

export default mongoose.model<IListings>("Listings", ListingsSchema);
